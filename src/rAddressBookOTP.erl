%%%-------------------------------------------------------------------
%%% @author Hubert
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. sty 2015 11:37
%%%-------------------------------------------------------------------
-module(rAddressBookOTP).
-author("Hubert").
-behaviour(gen_server).
%% API
-export([terminate/2, stop/0, crash/0, init/1, start_link/0, handle_cast/2, handle_info/2, code_change/3, findByPhone/1, findByEmail/1, getPhones/2, getEmails/2, removePhone/1, removeEmail/1, removeContact/2, addPhone/3, addEmail/3, addContact/2, handle_call/3]).


start_link() ->
  gen_server:start_link({local, rAddressBookOTP}, ?MODULE, [], []).

init([]) ->
  {ok, []}.

crash() ->
  gen_server:cast(rAddressBookOTP, crash).

stop() ->
  gen_server:cast(rAddressBookOTP, stop).

terminate(_Reason, _Value) ->
  ok.

handle_cast(stop, Book) ->
  {stop, normal, Book};
handle_cast(crash, Book) ->
  1 / 0,
  {noreply, Book}.

handle_info(_Message, Book) ->
  {noreply, Book}.

code_change(_OldVsn, Book, _Extra) ->
  {ok, Book}.


addContact(Name, Surname) ->
  gen_server:call(rAddressBookOTP, {addContact, Name, Surname}).

addEmail(Name, Surname, {Category, Email}) ->
  gen_server:call(rAddressBookOTP, {addEmail, Name, Surname, {Category, Email}}).

addPhone(Name, Surname, Phone) ->
  gen_server:call(rAddressBookOTP, {addPhone, Name, Surname, Phone}).

removeContact(Name, Surname) ->
  gen_server:call(rAddressBookOTP, {removeContact, Name, Surname}).

removeEmail(Email) ->
  gen_server:call(rAddressBookOTP, {removeEmail, Email}).

removePhone(Phone) ->
  gen_server:call(rAddressBookOTP, {removePhone, Phone}).

getEmails(Name, Surname) ->
  gen_server:call(rAddressBookOTP, {getEmails, Name, Surname}).

getPhones(Name, Surname) ->
  gen_server:call(rAddressBookOTP, {getPhones, Name, Surname}).

findByEmail(Email) ->
  gen_server:call(rAddressBookOTP, {findByEmail, Email}).

findByPhone(Phone) ->
  gen_server:call(rAddressBookOTP, {findByPhone, Phone}).

handle_call({addContact, Name, Surname}, _, Book) ->
  NewBook = addressbook:addContact(Name, Surname, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({addEmail, Name, Surname, Email}, _, Book) ->
  NewBook = addressbook:addEmail(Name, Surname, Email, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({addPhone, Name, Surname, Phone}, _, Book) ->
  NewBook = addressbook:addPhone(Name, Surname, Phone, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({removeContact, Name, Surname}, _, Book) ->
  NewBook = addressbook:removeContact(Name, Surname, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({removeEmail, {Category, Email}}, _, Book) ->
  NewBook = addressbook:removeEmail({Category, Email}, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({removePhone, Phone}, _, Book) ->
  NewBook = addressbook:removePhone(Phone, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    Book2 -> {reply, Book2, NewBook}
  end;
handle_call({getEmails, Name, Surname}, _, Book) ->
  NewBook = addressbook:getEmails(Name, Surname, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    _ -> {reply, NewBook, Book}
  end;
handle_call({getPhones, Name, Surname}, _, Book) ->
  NewBook = addressbook:getPhones(Name, Surname, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    _ -> {reply, NewBook, Book}
  end;
handle_call({findByEmail, Email}, _, Book) ->
  NewBook = addressbook:findByEmail(Email, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    _ -> {reply, NewBook, Book}
  end;
handle_call({findByPhone, Phone}, _, Book) ->
  NewBook = addressbook:findByPhone(Phone, Book),
  case NewBook of
    {error, A} -> {reply, {error, A}, Book};
    _ -> {reply, NewBook, Book}
  end.
