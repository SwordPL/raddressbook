%%%-------------------------------------------------------------------
%%% @author Hubert
%%% @copyright (C) 2014, AGH University of Science and Technology
%%% @doc
%%%   Server module for Addressbook Module
%%% @end
%%%-------------------------------------------------------------------
-module(raddressbook).
-author("Hubert").

%% API
-export([crash/0, addContact/2, addEmail/3, addPhone/3,
  removeContact/2, removeEmail/1, removePhone/1,
  getEmails/2, getPhones/2, findByEmail/1, findByPhone/1, stop/0]).
%% Server Implementation Details
-export([start/0, init/0, loop/1]).

start() ->
  register(server, spawn_link(?MODULE, init, [])).

init() ->
  loop(addressbook:createAddressBook()).

loop(AddressBook) ->
  receive
    {addContact, FName, LName, Pid} ->
      NewBook = addressbook:addContact(FName, LName, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {addEmail, FName, LName, {Category, Email}, Pid} ->
      NewBook = addressbook:addEmail(FName, LName, {Category, Email}, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {addPhone, FName, LName, Phone, Pid} ->
      NewBook = addressbook:addPhone(FName, LName, Phone, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {removeContact, FName, LName, Pid} ->
      NewBook = addressbook:removeContact(FName, LName, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {removeEmail, Email, Pid} ->
      NewBook = addressbook:removeEmail(Email, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {removePhone, Phone, Pid} ->
      NewBook = addressbook:removePhone(Phone, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {getEmails, FName, LName, Pid} ->
      NewBook = addressbook:getEmails(FName, LName, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {getPhones, FName, LName, Pid} ->
      NewBook = addressbook:getPhones(FName, LName, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {findByPhone, {Category, Email}, Pid} ->
      NewBook = addressbook:findByEmail({Category, Email}, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    {findByPhone, Phone, Pid} ->
      NewBook = addressbook:findByPhone(Phone, AddressBook),
      Pid ! NewBook,
      case NewBook of
        {error, _} -> loop(AddressBook);
        _ -> loop(NewBook)
      end;
    stop -> stop;
    crash -> 2 / 0
  end.

addContact(Name, Surname) ->
  server ! {addContact, Name, Surname, self()},
  receive
    Book -> Book
  end.

addEmail(Name, Surname, {Category, Email}) ->
  server ! {addEmail, Name, Surname, {Category, Email}, self()},
  receive
    Book -> Book
  end.

addPhone(Name, Surname, Phone) ->
  server ! {addPhone, Name, Surname, Phone, self()},
  receive
    Book -> Book
  end.

removeContact(Name, Surname) ->
  server ! {removeContact, Name, Surname, self()},
  receive
    Book -> Book
  end.

removeEmail(Email) ->
  server ! {removeEmail, Email, self()},
  receive
    Book -> Book
  end.

removePhone(Phone) ->
  server ! {removePhone, Phone, self()},
  receive
    Book -> Book
  end.

getEmails(Name, Surname) ->
  server ! {getEmails, Name, Surname, self()},
  receive
    Book -> Book
  end.

getPhones(Name, Surname) ->
  server ! {getPhones, Name, Surname, self()},
  receive
    Book -> Book
  end.

findByEmail(Email) ->
  server ! {findByEmail, Email, self()},
  receive
    Book -> Book
  end.

findByPhone(Phone) ->
  server ! {findByPhone, Phone, self()},
  receive
    Book -> Book
  end.

stop() -> server ! stop.

crash() ->
  server ! crash.
