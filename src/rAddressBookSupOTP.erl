%%%-------------------------------------------------------------------
%%% @author Hubert
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. sty 2015 11:45
%%%-------------------------------------------------------------------
-module(rAddressBookSupOTP).
-author("Hubert").
-behaviour(supervisor).
%% API
-export([init/1, start_link/0]).

start_link() ->
  supervisor:start_link({local, addressBookSupOTP}, ?MODULE, []).

init(_) ->
  {ok, {{one_for_all, 1, 100}, [{rAddressBookOTP, {rAddressBookOTP, start_link, []}, permanent, brutal_kill, worker, [rAddressBookOTP]}]}}.
