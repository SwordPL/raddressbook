%%%-------------------------------------------------------------------
%%% @author Hubert
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 17. gru 2014 10:47
%%%-------------------------------------------------------------------
-module(rAddressBookSup).
-author("Hubert").

%% API
-export([start/0, init/0]).

start() ->
  spawn(?MODULE, init, []).

init() ->
  process_flag(trap_exit, true),
  raddressbook:start(),
  receive
    {'EXIT', _, _} -> rAddressBookSup:init()
  end.